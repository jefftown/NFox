﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace NFox.WPF
{
    /// <summary>
    /// ViewModel基类
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public class ViewModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// 属性值更改事件。
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 属性改变时调用
        /// </summary>
        /// <param name="propertyName">属性名</param>
        public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        
    }
}
