

# WPF支持

## 一、只要在项目文件里添加如下代码即可支持wpf

```xaml
<ItemGroup>
    <!--WPF支持-->
    <Reference Include="PresentationCore" />
    <Reference Include="PresentationFramework" />
    <Reference Include="System.Xaml" />
    <Reference Include="WindowsBase" />
    <Page Include="**\*.xaml" Exclude="App.xaml" SubType="Designer" Generator="MSBuild:Compile" />
    <Compile Update="**\*.xaml.cs" DependentUpon="%(Filename)" />
    <EmbeddedResource Update="Properties\Resources.resx" Generator="ResXFileCodeGenerator" LastGenOutput="Resources.Designer.cs" />
    <Compile Update="Properties\Resources.Designer.cs" AutoGen="True" DependentUpon="Resources.resx" DesignTime="True" />
    <None Update="Properties\Settings.settings" Generator="SettingsSingleFileGenerator" LastGenOutput="Settings.Designer.cs" />
    <Compile Update="Properties\Settings.Designer.cs" AutoGen="True" DependentUpon="Settings.settings" />
  </ItemGroup>
```

## 二、在项目中创建wpf窗体

由于在类库项目内，只能添加wpf的自定义控件项目。如下图，你是不能添加wpf窗体到类库项目的。

![img](./png/wpf-9.png)

因此为了将wpf窗体添加到类库项目里，需要采用一些奇淫巧技。

1. 首先打开vs创建一个wpf应用项目。

![img](png/wpf-1.png)

2. 创建完之后什么，可以自定义一些内容，比如添加一些属性啊什么的，本示例保持默认。

![img](png/wpf-2.png)

3. 然后菜单栏-项目-导出模版，打开导出模版向导。

![img](png/wpf-3.png)

4. 选择项模版，下一步

![wpf-4](png/wpf-4.png)

5. 只选择MainWindow.xaml文件，下一步

![wpf-5](png/wpf-5.png)

6. 选择项引用，什么都不选，直接下一步。

![wpf-6](png/wpf-6.png)

7. 在模版选项里填写模版名称、说明，还可以添加图标，然后点击完成。

![wpf-7](png/wpf-7.png)

现在可以在类库里添加的模版就创建好了，下面是怎么使用这个模版在类库里插入wpf窗体。首先以frameworks类库项目为例：

1. 新建个类库项目，然后右键添加-新建项。

![wpf-8](png/wpf-8.png)

2. 然后选择刚刚新建的模版，点击添加。

![img](png/wpf-10.png)

3. 然后记得添加 PresentationCore、PresentationFramework、System.Xaml、WindowsBase这四个引用。

![img](png/wpf-11.png)

下面是standard类库项目：

1. 现在新建个standard类库项目，然后将第一条里的内容添加到项目文件里。然后保存。

![img](png/2.png)

2. 然后项目右键-添加-新建项，选择MyWindow，点击添加

![img](png/3.png)

3. 然后就会在类库里添加了window类型的wpf窗体了。

![img](png/4.png)

# mvvm模式支持

使用WPF的最佳实践就是采用mvvm模式，为了支持在cad插件里使用mvvm，NFox内裤定义了两个简单基类来完成属性通知和命令定义。当然这是一种及其简单的mvvm模式的支持，你还要自己手动来写大部分的代码来实现完整的mvvm模式。

要实现mvvm模式，要新建一个XXXView文件，一个XXXViewModel文件。我们应该采用一种通用的命名约定，即所有的gui显示都有XXXView来完成，而所有的业务逻辑都由XXXViewModel来完成。下面以一个具体的示例来说明怎么在cad的插件里使用mvvm模式。

1. 将我们上一节建立的MyWindow1文件改名为MyWindowView，然后将涉及到的类名也全部更改为MyWindowView。

2. 然后将MyWindowView.xaml文件的内容改为：

```xaml
<Window x:Class="Test.MyWindowView"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:Test"
        mc:Ignorable="d"
        Title="MyWindow1" Height="450" Width="800">
    <Grid>
        <StackPanel>
            <TextBox></TextBox>
            <Button>click</Button>
        </StackPanel>
    </Grid>
</Window>
```

就是添加了一个文本框，一个按钮。

3. 新建MyWindowViewModel.cs文件，内容如下：

```c#
using NFox.WPF; // 这里引入NFox.WPF命名空间，以便可以使用ViewModelBase和RelayCommand

namespace Test
{
    class MyWindowViewModel : ViewModelBase
    {
    	// 定义一个属性用于在文本框里显示
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                this.OnPropertyChanged();
            }
        }
				
		// 定义一个命令用于按钮的点击动作
        private RelayCommand clickCommand;
        public RelayCommand ClickCommand
        {
            get
            {
                if (clickCommand == null)
                {
                    clickCommand = new RelayCommand(
                        o => Name = "hello " + Name,  // 定义要执行的行为
                        e => {return !string.IsNullOrEmpty(Name);}); // 定义命令是否可用
                }
                return clickCommand;
            }
        }
		// 初始化Name属性为 World
        public MyWindowViewModel()
        {
            Name = "World";
        }
    }
}

```

这里需要注意的是，定义的属性是为了将属性绑定到文本框的Text属性上，这个叫做数据绑定。然后wpf里对于我们winform里的事件其实采用的更高级一些的命令来完成。本示例，定义的命令也是一个属性，这个属性返回一个RelayCommand对象的实例，这是实例的初始化函数包括两个部分，一个是要执行的动作，第二个是确定什么条件下按钮是不可用的，这个是通过命令是否可用来完成，是要命令是不能执行的，wpf会自动将控件切换为不可用状态，使其不可点击。

4. 现在回过头来对在xaml里将刚刚的viewmodel里定义的属性和命令绑定到控件上。

```xaml
<TextBox Text="{Binding Name,UpdateSourceTrigger=PropertyChanged}"></TextBox>
<Button Command="{Binding ClickCommand}">click</Button>
```

将这两行代码替换一下。然后在后台代码里(MyWindowView.xaml.cs)添加一行代码将viewmodel绑定到view上。

```c#
public MyWindowView()
{
    InitializeComponent();
    DataContext = new MyWindowViewModel(); //这里将一个viewmodel的实例绑定到view的DataContext属性上。
}
```

5. 至此，一个简单的wpf的mvvm模式的代码就完成了，下面的代码演示了怎么在cad里显示这个wpf窗体。

```c#
[CommandMethod("test")]
public void Test()
{
   var test = new MyWindowView();
   Application.ShowModalWindow(test);
}
```

6. 最后，这个窗体的效果是，当你点击按钮时，文本框的文字前面会加上hello。当你将文本框的文字全部删除后，按钮会变成不可用状态。如果你在试验的时候没有这个效果，这是cad的延迟导致的。多删除几次试几次后就会如期运行。